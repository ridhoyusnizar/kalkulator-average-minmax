package com.example.ridho.calculator_minmaxavg.Calculator;

import android.os.Build;
import android.support.annotation.RequiresApi;

import java.util.Arrays;

public class Calculator {
    private int angka;
    private int angka2;

    public Calculator(){


    }
    public Calculator(int angka)
    {
        this.setAngka(angka);
        this.setAngka2(angka2);

    }

    public int getAngka() {
        return angka;
    }

    public void setAngka(int angka) {
        this.angka = angka;
    }

    public int getAngka2() {
        return angka2;
    }

    public void setAngka2(int angka2) {
        this.angka2 = angka2;
    }

    public double penambahan(int angka, int angka2){
    return angka + angka2;
    }

    public double pengurangan(int angka,int angka2){
        return angka - angka2;
    }

    public double perkalian(int angka,int angka2){
        return angka * angka2;
    }
    public float pembagian(float angka,float angka2){
        return angka / angka2;
    }



    public double average(int angka[]){
        double avg = 0;

        for(int i = 0;i<angka.length; i++)
        {
            avg=(avg+angka[i]);
        }
        return avg/angka.length;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public int maxValue(int angka[]){
        int max = Arrays.stream(angka).max().getAsInt();
        return max;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public double minValue(int angka[]){
        int min = Arrays.stream(angka).min().getAsInt();
        return min;
    }




}
