package com.example.ridho.calculator_minmaxavg.Calculator;

import android.os.Build;
import android.support.annotation.RequiresApi;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Main {
    public static BufferedReader B = new BufferedReader(new InputStreamReader(System.in));
    Scanner input = new Scanner(System.in);
    int angka,angka2;

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static void main(String[] args) {
        Main main = new Main();
        main.menu();

    }


    static int[] input() { // this method has to return int[]
        Scanner input = new Scanner(System.in);
        int data;
        System.out.print("Put lots of data : "); data=input.nextInt();

        int[] numbers = new int[data];
        for (int i = 0; i < numbers.length; i++) {
            System.out.print("Data "+(i+1) +": "); numbers[i] = input.nextInt();
        }
        return numbers; // return array here
    }



    @RequiresApi(api = Build.VERSION_CODES.N)
    public void menu()
    {
        int pilihan;
        try
        {
            do
            {
                System.out.print("\n1.Calculator\n");
                System.out.print("2.Other\n");
                System.out.print("0.Exit\n");
                System.out.print("Enter Options : "); pilihan = Integer.parseInt(B.readLine());

                if(pilihan == 1)
                {
                    menuCalculator();
                }
                else if(pilihan == 2)
                {
                    otherMenu();
                }

            }while(pilihan !=0);
        }catch(Exception e){}
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void otherMenu() {
        int pilihan;
        try
        {
            do
            {
                System.out.print("\n1.Average\n");
                System.out.print("2.Maximum \n");
                System.out.print("3.Minimum \n");
                System.out.print("0.Exit\n");
                System.out.print("Enter Options : "); pilihan = Integer.parseInt(B.readLine());

                if(pilihan == 1)
                {
                    average();
                }
                else if(pilihan == 2)
                {
                    maximum();
                }
                else if(pilihan == 3)
                {
                    minimum();
                }

            }while(pilihan !=0);
        }catch(Exception e){}
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void minimum() {
        Calculator calculator = new Calculator();
        int[] numbers = input();
        System.out.println("MINIMUM VALUE : "+calculator.minValue(numbers));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void maximum() {
        Calculator calculator = new Calculator();
        int[] numbers = input();
        System.out.println("MAXIMUM VALUE : "+calculator.maxValue(numbers));

    }

    private void average() {
        Calculator calculator = new Calculator();
        int[] numbers = input(); // get the returned array
        System.out.println(" AVERAGE :  "+calculator.average(numbers));

    }
    private void add()
    {
        Calculator calculator = new Calculator();
        System.out.print("Input  : ");
        angka = input.nextInt();
        System.out.print("Input  : ");
        angka2 = input.nextInt();
        System.out.println(angka + " + "+ angka2 + " = " + calculator.penambahan(angka,angka2) );
    }
    private void kurang()
    {
        Calculator calculator = new Calculator();
        System.out.print("Input  : ");
        angka = input.nextInt();
        System.out.print("Input  : ");
        angka2 = input.nextInt();
        System.out.println(angka + " - "+ angka2 + " = " + calculator.pengurangan(angka,angka2) );
    }
    private void bagi()
    {
        Calculator calculator = new Calculator();
        System.out.print("Input  : ");
        angka = input.nextInt();
        System.out.print("Input  : ");
        angka2 = input.nextInt();
        System.out.println(angka + " / "+ angka2 + " = " + calculator.pembagian(angka,angka2) );
    }
    private void kali()
    {
        Calculator calculator = new Calculator();
        System.out.print("Input  : "); angka = input.nextInt();
        System.out.print("Input  : "); angka2 = input.nextInt();
        System.out.println(angka + " * "+ angka2 + " = " + calculator.perkalian(angka,angka2) );
    }

    private void menuCalculator() {
        int option;
        do{
            System.out.println("Calculator ");
            System.out.println("1. +  ");
            System.out.println("2. -  ");
            System.out.println("3. /  ");
            System.out.println("4. *  ");
            System.out.print("Enter the Options : ");
            option = input.nextInt();

            if(option == 1)
            {
                add();
            }
            else if(option == 2)
            {
                kurang();
            }
            else if(option == 3)
            {
                bagi();
            }
            else if(option ==4)
            {
                kali();
            }
        }while(option !=0);
    }

}
